#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "Ex_FunctionPointers.h"


int main()
{
	std::cout << "Insert 2 numbers" << std::endl;
	int a = 0, b = 0;
	std::cin >> a;
	std::cin >> b;
	std::cout << "What operation do you want:\n1- Addition \n2- Substraction\n3- Multiply\n4-Divide\n0- Exit" << std::endl;
	int option = 0;
	std::cin >> option;

	switch (option)
		{
		case 1:
			std::cout << Ex::GetResult(Ex::adunare, a, b) << std::endl;
			break;
		case 2:
			std::cout << Ex::GetResult(Ex::scadere, a, b) << std::endl;
			break;
		case 3:
			std::cout << Ex::GetResult(Ex::inmultire, a, b) << std::endl;
			break;
		case 4:
			std::cout << Ex::GetResult(Ex::impartire, a, b) << std::endl;
			break;

		default:
			break;
		}	

}