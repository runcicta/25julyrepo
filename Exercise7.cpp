#include <iostream>

void counter(void)
{
	static int count = 0;
	int num = 1;
	count += num;
	std::cout << "Function was called:" << count << std::endl;
}

int main()
{
	counter();
	counter();
	counter();
	return 0;
}