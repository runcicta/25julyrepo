#pragma once

namespace Ex
{
    
    void PointerToFunction();

    // Declare Operation as a function that takes two double parameters and returns a double;
	typedef double(*Operation)(double , double);

	double adunare(double, double);
	double scadere(double, double);
	double inmultire(double, double);
	double impartire(double, double);

    // Implement a function that receives an Operation as input and return the result applied on param1 and param2
    double GetResult(Operation operation, double param1, double param2);
}
