#include <iostream>

typedef bool(*Comparator) (int a, int b);

bool ascendCompare(int a, int b)
{
	return a > b;
}

void sort(int a[], int size, Comparator x )
{
	for (int i = 0; i < size - 1; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (x(a[i], a[j]) == true)
			{
				int temp = a[j];
				a[j] = a[i];
				a[i] = temp;
			}
		}
	}

}

int main()
{
	int a[3] = { 3 , 2, 1 };
	std::cout << "The unsorted array is:" << std::endl;
	for (int i = 0; i < sizeof(a)/sizeof(int); i++)
	{
		std::cout << a[i] << " " << std::endl;
	}

	sort(a, sizeof(a)/sizeof(int), ascendCompare);
	std::cout << "The sorted array is:" << std::endl;
	for (int i = 0; i < sizeof(a)/sizeof(int); i++)
	{
		std::cout << a[i] << " " << std::endl;
	}
	
}