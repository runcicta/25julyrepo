#include <iostream>

int main()
{
	float a = 2.0f, b = 4.0f, expectedproduct = 8.0f;
	std::cout << "a = " <<  a << '\n' <<  "b = " << b << '\n' << "expectedproduct = " << expectedproduct << std::endl;
	float realproduct = a*b;
	if (realproduct == expectedproduct)
	{
		std::cout << "Product is equal with expected product" << std::endl;
	}
}